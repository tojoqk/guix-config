;;; config.scm -- Web server's config
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu))
(use-service-modules networking ssh web certbot admin)

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(define %reopen-nginx-logs
  #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
      (kill pid SIGUSR1)))

(operating-system
  (locale "en_US.utf8")
  (timezone "Asia/Tokyo")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "web")
  (users (cons* (user-account
                  (name "masaya")
                  (comment "Masaya Tojo")
                  (group "users")
                  (home-directory "/home/masaya")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  (packages
    (append
     (list (specification->package "nss-certs"))
     %base-packages))
  (services
    (append
     (list (service dhcp-client-service-type)
           (service iptables-service-type
                    (iptables-configuration
                      (ipv4-rules (local-file "iptables.rules"))
                      (ipv6-rules (local-file "ip6tables.rules"))))
           (service ntp-service-type)
           (service openssh-service-type
                    (openssh-configuration
                      (permit-root-login #f)
                      (password-authentication? #f)
                      (authorized-keys
                       `(("masaya" ,(local-file "masaya.pub"))))))
           (service nginx-service-type
                    (nginx-configuration
                      (server-blocks
                        (list
                         (nginx-server-configuration
                           (server-name '("tojo.tokyo"))
                           (ssl-certificate "/etc/letsencrypt/live/tojo.tokyo/fullchain.pem")
                           (ssl-certificate-key "/etc/letsencrypt/live/tojo.tokyo/privkey.pem")
                           (listen '("443 ssl" "[::]:443 ssl"))
                           (raw-content
                            (list "return 301 https://www.tojo.tokyo;")))
                         (nginx-server-configuration
                           (server-name '("www.tojo.tokyo"))
                           (ssl-certificate "/etc/letsencrypt/live/www.tojo.tokyo/fullchain.pem")
                           (ssl-certificate-key "/etc/letsencrypt/live/www.tojo.tokyo/privkey.pem")
                           (listen '("443 ssl" "[::]:443 ssl"))
                           (root "/srv/www")
                           (raw-content
                            (list "access_log /var/log/nginx/www/access.log;"
                                  "error_log /var/log/nginx/www/error.log;"
                                  "error_page 404 /404-not-found.html;")))))))
           (service certbot-service-type
                    (certbot-configuration
                      (email "masaya@tojo.tokyo")
                      (certificates
                        (list
                         (certificate-configuration
                           (domains '("tojo.tokyo"))
                           (deploy-hook %nginx-deploy-hook))
                         (certificate-configuration
                           (domains '("www.tojo.tokyo"))
                           (deploy-hook %nginx-deploy-hook))))))
           (simple-service 'rotate-nginx
                           rottlog-service-type
                           (list (log-rotation
                                   (frequency 'daily)
                                   (files (list "/var/log/nginx/access.log"
                                                "/var/log/nginx/error.log"
                                                "/var/log/nginx/www/access.log"
                                                "/var/log/nginx/www/error.log"))
                                   (options '("nostoredir"))
                                   (post-rotate %reopen-nginx-logs)))))
     %base-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/vda")
      (keyboard-layout keyboard-layout)))
  (swap-devices (list "/dev/vda2"))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "de03171e-695e-4d5c-8955-2ef0795c9130"
                     'ext4))
             (type "ext4"))
           %base-file-systems)))
