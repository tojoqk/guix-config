;;; config.scm -- Mail server's config
;;; Copyright © 2020 Masaya Tojo <masaya@tojo.tokyo>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (gnu)
             (gnu packages admin)
             (gnu packages ssh))
(use-service-modules networking ssh web certbot mail admin sysctl)

(define %nginx-deploy-hook
  (program-file
   "nginx-deploy-hook"
   #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
       (kill pid SIGHUP))))

(define %reopen-nginx-logs
  #~(let ((pid (call-with-input-file "/var/run/nginx/pid" read)))
      (kill pid SIGUSR1)))

(operating-system
  (locale "en_US.utf8")
  (timezone "Asia/Tokyo")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "mail")
  (groups (cons* (user-group (name "sftp-only"))
                 (user-group (name "vmail")
                             (id 2000))
                 %base-groups))
  (users (cons* (user-account
                  (name "masaya")
                  (comment "Masaya Tojo")
                  (group "users")
                  (home-directory "/home/masaya")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                (user-account
                  (name "nextcloud")
                  (comment "Nextcloud")
                  (group "sftp-only")
                  (home-directory "/mnt/blockstorage/nextcloud")
                  (shell #~(string-append #$shadow "/sbin/nologin"))
                  (supplementary-groups '()))
                (user-account
                  (name "vmail")
                  (uid 2000)
                  (comment "Virtual Mail Account")
                  (group "vmail")
                  (home-directory "/mnt/blockstorage/vmail")
                  (shell #~(string-append #$shadow "/sbin/nologin"))
                  (supplementary-groups '()))
                %base-user-accounts))
  (packages
    (append
     (list (specification->package "nss-certs")
           (specification->package "opensmtpd-extras"))
     %base-packages))
  (services
    (append
     (list (service openssh-service-type
                    (openssh-configuration
                      (permit-root-login #f)
                      (password-authentication? #f)
                      (authorized-keys
                       `(("masaya" ,(local-file "masaya.pub"))
                         ("nextcloud" ,(local-file "nextcloud.pub"))))
                      (subsystems
                       `(("sftp" ,(file-append openssh "/libexec/sftp-server")))) (extra-content "\
Match Group sftp-only
  ChrootDirectory %h
  ForceCommand internal-sftp
  AllowTcpForwarding no
  X11Forwarding no
  PasswordAuthentication no
")))
           (service dhcp-client-service-type)
           (service iptables-service-type
                    (iptables-configuration
                      (ipv4-rules (local-file "iptables.rules"))
                      (ipv6-rules (local-file "ip6tables.rules"))))
           (service ntp-service-type)
           (service nginx-service-type
                    (nginx-configuration
                      (server-blocks
                        (list
                         (nginx-server-configuration
                           (server-name '("mail.tojo.tokyo"))
                           (listen '("443 ssl" "[::]:443 ssl"))
                           (ssl-certificate "/etc/letsencrypt/live/mail.tojo.tokyo/fullchain.pem")
                           (ssl-certificate-key "/etc/letsencrypt/live/mail.tojo.tokyo/privkey.pem")
                           (raw-content
                            (list "return 200 'OK';")))))))
           (service opensmtpd-service-type
                    (opensmtpd-configuration
                     (config-file (plain-file "smtpd.conf" "
pki mail.tojo.tokyo cert \"/etc/letsencrypt/live/mail.tojo.tokyo/fullchain.pem\"
pki mail.tojo.tokyo key \"/etc/letsencrypt/live/mail.tojo.tokyo/privkey.pem\"

table aliases     file:/mnt/blockstorage/etc/mail/aliases
table credentials file:/mnt/blockstorage/etc/mail/credentials
table virtuals    file:/mnt/blockstorage/etc/mail/virtuals
table domains     file:/mnt/blockstorage/etc/mail/domains

listen on eth0 tls pki mail.tojo.tokyo
listen on eth0 port 587 tls-require pki mail.tojo.tokyo auth <credentials>

action receive maildir \"/mnt/blockstorage/vmail/%{dest.user}\" virtual <virtuals>
action send relay

match from any for domain <domains> action receive
match auth from any for any action send
"))))
           (dovecot-service
            #:config
            (dovecot-configuration
             (listen (list "*"))
             (mail-location "maildir:/mnt/blockstorage/vmail/%n")
             (ssl-cert "</etc/letsencrypt/live/mail.tojo.tokyo/fullchain.pem")
             (ssl-key "</etc/letsencrypt/live/mail.tojo.tokyo/privkey.pem")
             (userdbs
              (list
               (userdb-configuration
                (driver "passwd-file")
                (args (list "username_format=%u"
                            "/mnt/blockstorage/etc/mail/passwd")))))
             (passdbs
              (list
               (passdb-configuration
                (driver "passwd-file")
                (args (list "scheme=CRYPT"
                            "username_format=%u"
                            "/mnt/blockstorage/etc/mail/passwd")))))))
           (service certbot-service-type
                    (certbot-configuration
                      (email "masaya@tojo.tokyo")
                      (certificates
                        (list
                         (certificate-configuration
                           (domains '("mail.tojo.tokyo"))
                           (deploy-hook %nginx-deploy-hook))))))
           (service sysctl-service-type
                    (sysctl-configuration
                     (settings '(("net.ipv6.conf.all.disable_ipv6" . "1")
                                 ("net.ipv6.conf.all.disable_ipv6" . "1")))))
           (simple-service 'rotate-nginx
                           rottlog-service-type
                           (list (log-rotation
                                   (frequency 'daily)
                                   (files (list "/var/log/nginx/access.log"
                                                "/var/log/nginx/error.log"))
                                   (options '("nostoredir"))
                                   (post-rotate %reopen-nginx-logs)))))
     %base-services))
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/vda")
      (keyboard-layout keyboard-layout)))
  (swap-devices (list "/dev/vda2"))
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device
               (uuid "de03171e-695e-4d5c-8955-2ef0795c9130"
                     'ext4))
             (type "ext4"))
           (file-system
             (mount-point "/mnt/blockstorage")
             (device
               (uuid "3e9fa18f-a6bc-480e-8a13-a29859bfa7e0"
                     'ext4))
             (type "ext4"))
           %base-file-systems)))
